package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Repositorios.EnvioTerrestreRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;

public class GeneradorNumeroGuiaServiceImplTest {

    @Mock
    private EnvioTerrestreRepository envioTerrestreRepository;

    @InjectMocks
    private GeneradorNumeroGuiaServiceImpl generadorNumeroGuiaService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test generateUniqueGuid")
    void testGenerateUniqueGuid() {
        Set<String> existingGuids = new HashSet<>();
        existingGuids.add("AAAABBBBCC");
        existingGuids.add("1234567890");

        when(envioTerrestreRepository.existsByNumeroGuia("AAAABBBBCC")).thenReturn(true);
        when(envioTerrestreRepository.existsByNumeroGuia("1234567890")).thenReturn(false);

        String uniqueGuid1 = generadorNumeroGuiaService.generateUniqueGuid();
        String uniqueGuid2 = generadorNumeroGuiaService.generateUniqueGuid();

        Assertions.assertNotEquals("AAAABBBBCC", uniqueGuid1);
        Assertions.assertNotEquals("1234567890", uniqueGuid2);
    }
}
