package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Modelo.DTO.ClienteDTO;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Repositorios.ClienteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class ClienteServiceImplTest {

    @Mock
    private ClienteRepository clienteRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private ClienteServiceImpl clienteService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllClientes() {
        List<Cliente> clientes = new ArrayList<>();
        when(clienteRepository.findAll()).thenReturn(clientes);
        List<ClienteDTO> expected = new ArrayList<>();
        when(modelMapper.map(any(Cliente.class), eq(ClienteDTO.class))).thenReturn(new ClienteDTO());

        List<ClienteDTO> result = clienteService.getAllClientes();

        assertEquals(expected, result);
    }

    @Test
    void testGetClienteById() {
        Long id = 1L;
        Cliente cliente = new Cliente();
        when(clienteRepository.findById(id)).thenReturn(Optional.of(cliente));
        when(modelMapper.map(any(Cliente.class), eq(ClienteDTO.class))).thenReturn(new ClienteDTO());

        ClienteDTO result = clienteService.getClienteById(id);

        assertNotNull(result);
    }

    @Test
    void testSaveCliente() {
        ClienteDTO clienteDTO = new ClienteDTO();
        Cliente cliente = new Cliente();
        when(modelMapper.map(clienteDTO, Cliente.class)).thenReturn(cliente);
        when(passwordEncoder.encode(anyString())).thenReturn("hashedPassword");
        when(clienteRepository.save(cliente)).thenReturn(cliente);
        when(modelMapper.map(cliente, ClienteDTO.class)).thenReturn(clienteDTO);

        ClienteDTO result = clienteService.saveCliente(clienteDTO);

        assertNotNull(result);
    }

    @Test
    void testDeleteCliente() {
        Long id = 1L;

        clienteService.deleteCliente(id);

        verify(clienteRepository, times(1)).deleteById(id);
    }
}
