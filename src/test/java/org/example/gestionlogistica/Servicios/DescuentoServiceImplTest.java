package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Configuracion.Constantes.Constantes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class DescuentoServiceImplTest {

    @Mock
    private Constantes constantes;

    @InjectMocks
    private DescuentoServiceImpl descuentoService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test calcularPrecioConDescuento - Terrestrial and Quantity Greater Than 10")
    void testCalcularPrecioConDescuentoTerrestrialAndQuantityGreaterThan10() {
        // Given
        String precioEnvio = "95.0";
        int cantidad = 15;
        String tipoEnvio = "Terrestrial";

        // When
        String precioCalculado = descuentoService.calcularPrecioConDescuento(precioEnvio, cantidad, tipoEnvio);

        // Then
        Assertions.assertEquals("95.0", precioCalculado);
    }

    @Test
    @DisplayName("Test calcularPrecioConDescuento with maritime shipping and quantity less than or equal to 10")
    void testCalcularPrecioConDescuentoMaritimeAndQuantityLessThanOrEqualTo10() {
        // Given
        String precioEnvio = "100.0";
        int cantidad = 10;
        String tipoEnvio = "Maritime";

        // When
        String precioFinal = descuentoService.calcularPrecioConDescuento(precioEnvio, cantidad, tipoEnvio);

        // Then
        Assertions.assertEquals("100.0", precioFinal);
    }

    @Test
    @DisplayName("Test calcularPrecioConDescuento with unknown shipping type")
    void testCalcularPrecioConDescuentoUnknownShippingType() {
        // Given
        String precioEnvio = "100.0";
        int cantidad = 5;
        String tipoEnvio = "Unknown";

        // When
        String precioFinal = descuentoService.calcularPrecioConDescuento(precioEnvio, cantidad, tipoEnvio);

        // Then
        Assertions.assertEquals("100.0", precioFinal);
    }
}
