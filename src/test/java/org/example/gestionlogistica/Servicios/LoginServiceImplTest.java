package org.example.gestionlogistica.Servicios;

import jakarta.servlet.http.HttpServletRequest;
import org.example.gestionlogistica.Configuracion.Constantes.Constantes;
import org.example.gestionlogistica.Configuracion.Exceptiones.UsuarioNoEncontradoException;
import org.example.gestionlogistica.Configuracion.JwtUtils;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Repositorios.ClienteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.naming.AuthenticationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

public class LoginServiceImplTest {

    @Mock
    private ClienteRepository clienteRepository;

    @Mock
    private JwtUtils jwtUtils;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private LoginServiceImpl loginService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    @DisplayName("Test login with invalid credentials")
    void testLoginWithInvalidCredentials() {
        // Given
        String correo = "example@example.com";
        String contrasena = "password";
        String encodedPassword = passwordEncoder.encode(contrasena);
        Cliente cliente = new Cliente();
        cliente.setCorreo(correo);
        cliente.setContrasena(encodedPassword);

        when(clienteRepository.findByCorreo(correo)).thenReturn(cliente);
        when(passwordEncoder.matches(contrasena, encodedPassword)).thenReturn(false);

        // Then
        assertThrows(UsuarioNoEncontradoException.class, () -> loginService.login(correo, contrasena));
    }

}
