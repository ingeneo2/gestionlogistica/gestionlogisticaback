package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Modelo.DTO.PuertoDTO;
import org.example.gestionlogistica.Modelo.Entidades.Puerto;
import org.example.gestionlogistica.Repositorios.PuertoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class PuertoServiceImplTest {

    @Mock
    private PuertoRepository puertoRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private PuertoServiceImpl puertoService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllPuertos() {
        List<Puerto> puertos = new ArrayList<>();
        when(puertoRepository.findAll()).thenReturn(puertos);
        List<PuertoDTO> expected = new ArrayList<>();
        when(modelMapper.map(any(Puerto.class), eq(PuertoDTO.class))).thenReturn(new PuertoDTO());

        List<PuertoDTO> result = puertoService.getAllPuertos();

        assertEquals(expected, result);
    }

    @Test
    void testGetPuertoById() {
        Long id = 1L;
        Puerto puerto = new Puerto();
        when(puertoRepository.findById(id)).thenReturn(Optional.of(puerto));
        when(modelMapper.map(any(Puerto.class), eq(PuertoDTO.class))).thenReturn(new PuertoDTO());

        PuertoDTO result = puertoService.getPuertoById(id);

        assertNotNull(result);
    }

    @Test
    void testSavePuerto() {
        PuertoDTO puertoDTO = new PuertoDTO();
        Puerto puerto = new Puerto();
        when(modelMapper.map(puertoDTO, Puerto.class)).thenReturn(puerto);
        when(puertoRepository.save(puerto)).thenReturn(puerto);
        when(modelMapper.map(puerto, PuertoDTO.class)).thenReturn(puertoDTO);

        PuertoDTO result = puertoService.savePuerto(puertoDTO);

        assertNotNull(result);
    }

    @Test
    void testDeletePuerto() {
        Long id = 1L;

        puertoService.deletePuerto(id);

        verify(puertoRepository, times(1)).deleteById(id);
    }
}
