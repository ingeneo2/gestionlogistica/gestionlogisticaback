package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Modelo.DTO.ClienteDTO;
import org.example.gestionlogistica.Modelo.DTO.EnvioMaritimoDTO;
import org.example.gestionlogistica.Modelo.DTO.ProductoDTO;
import org.example.gestionlogistica.Modelo.DTO.PuertoDTO;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Modelo.Entidades.EnvioMaritimo;
import org.example.gestionlogistica.Modelo.Entidades.Producto;
import org.example.gestionlogistica.Modelo.Entidades.Puerto;
import org.example.gestionlogistica.Repositorios.ClienteRepository;
import org.example.gestionlogistica.Repositorios.EnvioMaritimoRepository;
import org.example.gestionlogistica.Repositorios.ProductoRepository;
import org.example.gestionlogistica.Repositorios.PuertoRepository;
import org.example.gestionlogistica.Servicios.EnvioMaritimoServiceImpl;
import org.example.gestionlogistica.Servicios.GeneradorNumeroGuiaServiceImpl;
import org.example.gestionlogistica.Servicios.DescuentoServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EnvioMaritimoServiceTest {

    @Mock
    private EnvioMaritimoRepository envioMaritimoRepository;

    @Mock
    private GeneradorNumeroGuiaServiceImpl generadorNumeroGuia;

    @Mock
    private DescuentoServiceImpl calculadoraDescuento;

    @Mock
    private ProductoRepository productoRepository;

    @Mock
    private ClienteRepository clienteRepository;

    @Mock
    private PuertoRepository puertoRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private EnvioMaritimoServiceImpl envioMaritimoService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test getAllEnviosMaritimos")
    void testGetAllEnviosMaritimos() {
        // Given
        List<EnvioMaritimo> enviosMaritimos = new ArrayList<>();
        enviosMaritimos.add(new EnvioMaritimo());
        enviosMaritimos.add(new EnvioMaritimo());
        when(envioMaritimoRepository.findAll()).thenReturn(enviosMaritimos);

        List<EnvioMaritimoDTO> enviosMaritimosDTO = new ArrayList<>();
        enviosMaritimosDTO.add(new EnvioMaritimoDTO());
        enviosMaritimosDTO.add(new EnvioMaritimoDTO());
        when(modelMapper.map(any(), any())).thenReturn(enviosMaritimosDTO.get(0), enviosMaritimosDTO.get(1));

        // When
        List<EnvioMaritimoDTO> result = envioMaritimoService.getAllEnviosMaritimos();

        // Then
        Assertions.assertEquals(2, result.size());
    }

    @Test
    @DisplayName("Test getEnvioMaritimoById")
    void testGetEnvioMaritimoById() {
        // Given
        EnvioMaritimo envioMaritimo = new EnvioMaritimo();
        when(envioMaritimoRepository.findById(1L)).thenReturn(Optional.of(envioMaritimo));

        EnvioMaritimoDTO envioMaritimoDTO = new EnvioMaritimoDTO();
        when(modelMapper.map(envioMaritimo, EnvioMaritimoDTO.class)).thenReturn(envioMaritimoDTO);

        // When
        EnvioMaritimoDTO result = envioMaritimoService.getEnvioMaritimoById(1L);

        // Then
        Assertions.assertNotNull(result);
    }

    @Test
    @DisplayName("Test saveEnvioMaritimo")
    void testSaveEnvioMaritimo() {
        // Given
        EnvioMaritimoDTO envioMaritimoDTO = new EnvioMaritimoDTO();
        envioMaritimoDTO.setCliente(new ClienteDTO());
        envioMaritimoDTO.setProducto(new ProductoDTO());
        envioMaritimoDTO.setPuerto(new PuertoDTO());

        Cliente cliente = new Cliente();
        Producto producto = new Producto();
        Puerto puerto = new Puerto();
        when(clienteRepository.findByNombre(any())).thenReturn(cliente);
        when(productoRepository.findByNombre(any())).thenReturn(producto);
        when(puertoRepository.findByNombreAndUbicacion(any(), any())).thenReturn(puerto);

        EnvioMaritimo envioMaritimo = new EnvioMaritimo();
        when(modelMapper.map(envioMaritimoDTO, EnvioMaritimo.class)).thenReturn(envioMaritimo);
        when(envioMaritimoRepository.save(envioMaritimo)).thenReturn(envioMaritimo);


    }

    @Test
    @DisplayName("Test deleteEnvioMaritimo")
    void testDeleteEnvioMaritimo() {
        // When
        Assertions.assertDoesNotThrow(() -> envioMaritimoService.deleteEnvioMaritimo(1L));
    }
}
