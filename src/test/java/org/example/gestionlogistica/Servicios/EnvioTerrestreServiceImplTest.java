package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Modelo.DTO.BodegaDTO;
import org.example.gestionlogistica.Modelo.DTO.ClienteDTO;
import org.example.gestionlogistica.Modelo.DTO.EnvioTerrestreDTO;
import org.example.gestionlogistica.Modelo.DTO.ProductoDTO;
import org.example.gestionlogistica.Modelo.Entidades.Bodega;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Modelo.Entidades.EnvioTerrestre;
import org.example.gestionlogistica.Modelo.Entidades.Producto;
import org.example.gestionlogistica.Repositorios.BodegaRepository;
import org.example.gestionlogistica.Repositorios.ClienteRepository;
import org.example.gestionlogistica.Repositorios.EnvioTerrestreRepository;
import org.example.gestionlogistica.Repositorios.ProductoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EnvioTerrestreServiceImplTest {

    @Mock
    private EnvioTerrestreRepository envioTerrestreRepository;

    @Mock
    private GeneradorNumeroGuiaServiceImpl generadorNumeroGuia;

    @Mock
    private DescuentoServiceImpl calculadoraDescuento;

    @Mock
    private BodegaRepository bodegaRepository;

    @Mock
    private ProductoRepository productoRepository;

    @Mock
    private ClienteRepository clienteRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private EnvioTerrestreServiceImpl envioTerrestreService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test getAllEnviosTerrestres")
    void testGetAllEnviosTerrestres() {
        List<EnvioTerrestre> enviosTerrestres = new ArrayList<>();
        enviosTerrestres.add(new EnvioTerrestre());
        enviosTerrestres.add(new EnvioTerrestre());
        when(envioTerrestreRepository.findAll()).thenReturn(enviosTerrestres);

        List<EnvioTerrestreDTO> enviosTerrestresDTO = new ArrayList<>();
        enviosTerrestresDTO.add(new EnvioTerrestreDTO());
        enviosTerrestresDTO.add(new EnvioTerrestreDTO());
        when(modelMapper.map(any(), any())).thenReturn(enviosTerrestresDTO.get(0), enviosTerrestresDTO.get(1));

        List<EnvioTerrestreDTO> result = envioTerrestreService.getAllEnviosTerrestres();

        Assertions.assertEquals(2, result.size());
    }

    @Test
    @DisplayName("Test getEnvioTerrestreById")
    void testGetEnvioTerrestreById() {
        EnvioTerrestre envioTerrestre = new EnvioTerrestre();
        when(envioTerrestreRepository.findById(1L)).thenReturn(Optional.of(envioTerrestre));

        EnvioTerrestreDTO envioTerrestreDTO = new EnvioTerrestreDTO();
        when(modelMapper.map(envioTerrestre, EnvioTerrestreDTO.class)).thenReturn(envioTerrestreDTO);

        EnvioTerrestreDTO result = envioTerrestreService.getEnvioTerrestreById(1L);

        Assertions.assertNotNull(result);
    }

    @Test
    @DisplayName("Test saveEnvioTerrestre")
    void testSaveEnvioTerrestre() {
        EnvioTerrestreDTO envioTerrestreDTO = new EnvioTerrestreDTO();
        envioTerrestreDTO.setCliente(new ClienteDTO());
        envioTerrestreDTO.setProducto(new ProductoDTO());
        envioTerrestreDTO.setBodega(new BodegaDTO());

        Cliente cliente = new Cliente();
        Producto producto = new Producto();
        Bodega bodega = new Bodega();
        when(clienteRepository.findByNombre(any())).thenReturn(cliente);
        when(productoRepository.findByNombre(any())).thenReturn(producto);
        when(bodegaRepository.findByNombreAndUbicacion(any(), any())).thenReturn(bodega);

        EnvioTerrestre envioTerrestre = new EnvioTerrestre();
        when(modelMapper.map(envioTerrestreDTO, EnvioTerrestre.class)).thenReturn(envioTerrestre);
        when(envioTerrestreRepository.save(envioTerrestre)).thenReturn(envioTerrestre);


    }

    @Test
    @DisplayName("Test deleteEnvioTerrestre")
    void testDeleteEnvioTerrestre() {
        Assertions.assertDoesNotThrow(() -> envioTerrestreService.deleteEnvioTerrestre(1L));
    }
}
