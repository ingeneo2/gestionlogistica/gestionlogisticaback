package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Modelo.DTO.ProductoDTO;
import org.example.gestionlogistica.Modelo.Entidades.Producto;
import org.example.gestionlogistica.Repositorios.ProductoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ProductoServiceTest {

    @Mock
    private ProductoRepository productoRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private ProductoServiceImpl productoService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Test getAllProductos")
    void testGetAllProductos() {
        List<Producto> productos = new ArrayList<>();
        productos.add(new Producto(1L, "Producto 1", "Descripción 1", "Tipo 1"));
        productos.add(new Producto(2L, "Producto 2", "Descripción 2", "Tipo 2"));
        when(productoRepository.findAll()).thenReturn(productos);

        List<ProductoDTO> productoDTOs = new ArrayList<>();
        productoDTOs.add(new ProductoDTO(1L, "Producto 1", "Descripción 1", "Tipo 1"));
        productoDTOs.add(new ProductoDTO(2L, "Producto 2", "Descripción 2", "Tipo 2"));
        when(modelMapper.map(any(), any())).thenReturn(productoDTOs.get(0), productoDTOs.get(1));


        List<ProductoDTO> result = productoService.getAllProductos();


        Assertions.assertEquals(2, result.size());

    }

    @Test
    @DisplayName("Test getProductoById")
    void testGetProductoById() {

        Producto producto = new Producto(1L, "Producto 1", "Descripción 1", "Tipo 1");
        when(productoRepository.findById(1L)).thenReturn(Optional.of(producto));

        ProductoDTO productoDTO = new ProductoDTO(1L, "Producto 1", "Descripción 1", "Tipo 1");
        when(modelMapper.map(any(), any())).thenReturn(productoDTO);


        ProductoDTO result = productoService.getProductoById(1L);


    }

    @Test
    @DisplayName("Test saveProducto")
    void testSaveProducto() {
        ProductoDTO productoDTO = new ProductoDTO(1L, "Producto 1", "Descripción 1", "Tipo 1");
        Producto producto = new Producto(1L, "Producto 1", "Descripción 1", "Tipo 1");
        when(modelMapper.map(any(), any())).thenReturn(producto);
        when(productoRepository.save(any())).thenReturn(producto);

    }

    @Test
    @DisplayName("Test deleteProducto")
    void testDeleteProducto() {
        // When
        Assertions.assertDoesNotThrow(() -> productoService.deleteProducto(1L));
    }
}
