package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Modelo.DTO.BodegaDTO;
import org.example.gestionlogistica.Modelo.Entidades.Bodega;
import org.example.gestionlogistica.Repositorios.BodegaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class BodegaServiceImplTest {

    @Mock
    private BodegaRepository bodegaRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private BodegaServiceImpl bodegaService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllBodegas() {
        List<Bodega> bodegas = new ArrayList<>();
        when(bodegaRepository.findAll()).thenReturn(bodegas);
        List<BodegaDTO> expected = new ArrayList<>();
        when(modelMapper.map(any(Bodega.class), eq(BodegaDTO.class))).thenReturn(new BodegaDTO());

        List<BodegaDTO> result = bodegaService.getAllBodegas();

        assertEquals(expected, result);
    }

    @Test
    void testGetBodegaById() {
        Long id = 1L;
        Bodega bodega = new Bodega();
        when(bodegaRepository.findById(id)).thenReturn(Optional.of(bodega));
        when(modelMapper.map(any(Bodega.class), eq(BodegaDTO.class))).thenReturn(new BodegaDTO());

        BodegaDTO result = bodegaService.getBodegaById(id);

        assertNotNull(result);
    }

    @Test
    void testSaveBodega() {
        BodegaDTO bodegaDTO = new BodegaDTO();
        Bodega bodega = new Bodega();
        when(modelMapper.map(bodegaDTO, Bodega.class)).thenReturn(bodega);
        when(bodegaRepository.save(bodega)).thenReturn(bodega);
        when(modelMapper.map(bodega, BodegaDTO.class)).thenReturn(bodegaDTO);

        BodegaDTO result = bodegaService.saveBodega(bodegaDTO);

        assertNotNull(result);
    }

    @Test
    void testDeleteBodega() {
        Long id = 1L;

        bodegaService.deleteBodega(id);

        verify(bodegaRepository, times(1)).deleteById(id);
    }
}
