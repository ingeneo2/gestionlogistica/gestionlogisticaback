package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.EnvioTerrestreDTO;
import org.example.gestionlogistica.Servicios.EnvioTerrestreServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class EnvioTerrestreControllerTest {

    @Mock
    private EnvioTerrestreServiceImpl envioTerrestreService;

    @InjectMocks
    private EnvioTerrestreController envioTerrestreController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllEnviosTerrestres() {
        // Mock data
        List<EnvioTerrestreDTO> enviosTerrestres = new ArrayList<>();
        when(envioTerrestreService.getAllEnviosTerrestres()).thenReturn(enviosTerrestres);

        ResponseEntity<List<EnvioTerrestreDTO>> responseEntity = envioTerrestreController.getAllEnviosTerrestres();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(enviosTerrestres, responseEntity.getBody());
    }

    @Test
    void testGetEnvioTerrestreById() {
        long id = 1L;
        EnvioTerrestreDTO envioTerrestreDTO = new EnvioTerrestreDTO();
        when(envioTerrestreService.getEnvioTerrestreById(id)).thenReturn(envioTerrestreDTO);

        ResponseEntity<EnvioTerrestreDTO> responseEntity = envioTerrestreController.getEnvioTerrestreById(id);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(envioTerrestreDTO, responseEntity.getBody());
    }

    @Test
    void testSaveEnvioTerrestre() {
        EnvioTerrestreDTO envioTerrestreDTO = new EnvioTerrestreDTO();
        when(envioTerrestreService.saveEnvioTerrestre(envioTerrestreDTO)).thenReturn(envioTerrestreDTO);

        ResponseEntity<EnvioTerrestreDTO> responseEntity = envioTerrestreController.saveEnvioTerrestre(envioTerrestreDTO);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(envioTerrestreDTO, responseEntity.getBody());
    }

    @Test
    void testDeleteEnvioTerrestre() {
        long id = 1L;

        ResponseEntity<Void> responseEntity = envioTerrestreController.deleteEnvioTerrestre(id);

        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        verify(envioTerrestreService, times(1)).deleteEnvioTerrestre(id);
    }
}
