package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.PuertoDTO;
import org.example.gestionlogistica.Servicios.PuertoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class PuertoControllerTest {

    @Mock
    private PuertoServiceImpl puertoService;

    @InjectMocks
    private PuertoController puertoController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllPuertos() {
        List<PuertoDTO> puertos = Arrays.asList(
                new PuertoDTO(1L, "Puerto 1", "Ubicación 1", 100),
                new PuertoDTO(2L, "Puerto 2", "Ubicación 2", 200)
        );
        when(puertoService.getAllPuertos()).thenReturn(puertos);

        ResponseEntity<List<PuertoDTO>> response = puertoController.getAllPuertos();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(puertos, response.getBody());
    }

    @Test
    void testGetPuertoById() {
        Long puertoId = 1L;
        PuertoDTO puertoDTO = new PuertoDTO(puertoId, "Puerto 1", "Ubicación 1", 100);
        when(puertoService.getPuertoById(puertoId)).thenReturn(puertoDTO);

        ResponseEntity<PuertoDTO> response = puertoController.getPuertoById(puertoId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(puertoDTO, response.getBody());
    }

    @Test
    void testSavePuerto() {
        PuertoDTO puertoDTO = new PuertoDTO(1L, "Puerto 1", "Ubicación 1", 100);
        when(puertoService.savePuerto(puertoDTO)).thenReturn(puertoDTO);

        ResponseEntity<PuertoDTO> response = puertoController.savePuerto(puertoDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(puertoDTO, response.getBody());
    }

    @Test
    void testDeletePuerto() {
        Long puertoId = 1L;

        ResponseEntity<Void> response = puertoController.deletePuerto(puertoId);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(puertoService, times(1)).deletePuerto(puertoId);
    }
}
