package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.EnvioMaritimoDTO;
import org.example.gestionlogistica.Servicios.EnvioMaritimoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class EnvioMaritimoControllerTest {

    @Mock
    private EnvioMaritimoServiceImpl envioMaritimoService;

    @InjectMocks
    private EnvioMaritimoController envioMaritimoController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllEnviosMaritimos() {
        List<EnvioMaritimoDTO> enviosMaritimos = new ArrayList<>();
        when(envioMaritimoService.getAllEnviosMaritimos()).thenReturn(enviosMaritimos);

        ResponseEntity<List<EnvioMaritimoDTO>> responseEntity = envioMaritimoController.getAllEnviosMaritimos();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(enviosMaritimos, responseEntity.getBody());
    }

    @Test
    void testGetEnvioMaritimoById() {
        long id = 1L;
        EnvioMaritimoDTO envioMaritimoDTO = new EnvioMaritimoDTO();
        when(envioMaritimoService.getEnvioMaritimoById(id)).thenReturn(envioMaritimoDTO);

        ResponseEntity<EnvioMaritimoDTO> responseEntity = envioMaritimoController.getEnvioMaritimoById(id);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(envioMaritimoDTO, responseEntity.getBody());
    }

    @Test
    void testSaveEnvioMaritimo() {
        EnvioMaritimoDTO envioMaritimoDTO = new EnvioMaritimoDTO();
        when(envioMaritimoService.saveEnvioMaritimo(envioMaritimoDTO)).thenReturn(envioMaritimoDTO);

        ResponseEntity<EnvioMaritimoDTO> responseEntity = envioMaritimoController.saveEnvioMaritimo(envioMaritimoDTO);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(envioMaritimoDTO, responseEntity.getBody());
    }

    @Test
    void testDeleteEnvioMaritimo() {
        long id = 1L;

        ResponseEntity<Void> responseEntity = envioMaritimoController.deleteEnvioMaritimo(id);

        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        verify(envioMaritimoService, times(1)).deleteEnvioMaritimo(id);
    }
}
