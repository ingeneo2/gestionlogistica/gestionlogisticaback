package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.BodegaDTO;
import org.example.gestionlogistica.Servicios.BodegaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class BodegaControllerTest {

    @Mock
    private BodegaServiceImpl bodegaService;

    @InjectMocks
    private BodegaController bodegaController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllBodegas() {
        // Arrange
        List<BodegaDTO> bodegas = Arrays.asList(
                new BodegaDTO(1L, "Bodega 1", "Ubicación 1", 100),
                new BodegaDTO(2L, "Bodega 2", "Ubicación 2", 200)
        );
        when(bodegaService.getAllBodegas()).thenReturn(bodegas);

        // Act
        ResponseEntity<List<BodegaDTO>> response = bodegaController.getAllBodegas();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(bodegas, response.getBody());
    }

    @Test
    void testGetBodegaById() {
        // Arrange
        Long bodegaId = 1L;
        BodegaDTO bodegaDTO = new BodegaDTO(bodegaId, "Bodega 1", "Ubicación 1", 100);
        when(bodegaService.getBodegaById(bodegaId)).thenReturn(bodegaDTO);

        // Act
        ResponseEntity<BodegaDTO> response = bodegaController.getBodegaById(bodegaId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(bodegaDTO, response.getBody());
    }

    @Test
    void testSaveBodega() {
        // Arrange
        BodegaDTO bodegaDTO = new BodegaDTO(1L, "Bodega 1", "Ubicación 1", 100);
        when(bodegaService.saveBodega(bodegaDTO)).thenReturn(bodegaDTO);

        // Act
        ResponseEntity<BodegaDTO> response = bodegaController.saveBodega(bodegaDTO);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(bodegaDTO, response.getBody());
    }

    @Test
    void testDeleteBodega() {
        // Arrange
        Long bodegaId = 1L;

        // Act
        ResponseEntity<Void> response = bodegaController.deleteBodega(bodegaId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(bodegaService, times(1)).deleteBodega(bodegaId);
    }
}
