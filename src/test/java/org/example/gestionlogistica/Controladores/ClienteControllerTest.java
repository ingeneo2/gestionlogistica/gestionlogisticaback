package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.ClienteDTO;
import org.example.gestionlogistica.Servicios.ClienteServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ClienteControllerTest {

    @InjectMocks
    private ClienteController clienteController;

    @Mock
    private ClienteServiceImpl clienteService;

    private ClienteDTO mockClienteDTO;
    private List<ClienteDTO> mockClienteList;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        mockClienteDTO = new ClienteDTO();
        mockClienteDTO.setId(1L);
        mockClienteDTO.setNombre("Cliente Test");

        mockClienteList = new ArrayList<>();
        mockClienteList.add(mockClienteDTO);
    }

    @Test
    public void testGetAllClientes_shouldReturnAllClientes() {
        when(clienteService.getAllClientes()).thenReturn(mockClienteList);

        ResponseEntity<List<ClienteDTO>> response = clienteController.getAllClientes();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockClienteList, response.getBody());
    }

    @Test
    public void testGetClienteById_shouldReturnClienteById() {
        when(clienteService.getClienteById(1L)).thenReturn(mockClienteDTO);

        ResponseEntity<ClienteDTO> response = clienteController.getClienteById(1L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockClienteDTO, response.getBody());
    }

    @Test
    public void testDeleteClienteAsync() {
        // Simular llamada a deleteCliente en un hilo diferente

        ResponseEntity<Void> response = clienteController.deleteCliente(1L);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(clienteService).deleteCliente(1L);
    }
}
