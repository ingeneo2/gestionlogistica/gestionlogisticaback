package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.ProductoDTO;
import org.example.gestionlogistica.Servicios.ProductoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ProductoControllerTest {

    @Mock
    private ProductoServiceImpl productoService;

    @InjectMocks
    private ProductoController productoController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllProductos() {
        List<ProductoDTO> productos = Arrays.asList(
                new ProductoDTO(1L, "Producto 1", "Descripción 1", "Tipo 1"),
                new ProductoDTO(2L, "Producto 2", "Descripción 2", "Tipo 2")
        );
        when(productoService.getAllProductos()).thenReturn(productos);

        ResponseEntity<List<ProductoDTO>> response = productoController.getAllProductos();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(productos, response.getBody());
    }

    @Test
    void testGetProductoById() {
        Long productoId = 1L;
        ProductoDTO productoDTO = new ProductoDTO(productoId, "Producto 1", "Descripción 1", "Tipo 1");
        when(productoService.getProductoById(productoId)).thenReturn(productoDTO);

        ResponseEntity<ProductoDTO> response = productoController.getProductoById(productoId);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(productoDTO, response.getBody());
    }

    @Test
    void testSaveProducto() {
        ProductoDTO productoDTO = new ProductoDTO(1L, "Producto 1", "Descripción 1", "Tipo 1");
        when(productoService.saveProducto(productoDTO)).thenReturn(productoDTO);

        ResponseEntity<ProductoDTO> response = productoController.saveProducto(productoDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(productoDTO, response.getBody());
    }

    @Test
    void testDeleteProducto() {

        Long productoId = 1L;


        ResponseEntity<Void> response = productoController.deleteProducto(productoId);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(productoService, times(1)).deleteProducto(productoId);
    }
}
