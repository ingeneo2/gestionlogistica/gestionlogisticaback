package org.example.gestionlogistica.Controladores;

import jakarta.servlet.http.HttpServletRequest;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Servicios.LoginServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class LoginControllerTest {

    @Mock
    private LoginServiceImpl loginService;

    @InjectMocks
    private LoginController loginController;

    @Mock
    private HttpServletRequest request;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testMe() {
        Cliente usuario = new Cliente();
        when(loginService.getCurrentUser(request)).thenReturn(usuario);

        ResponseEntity<?> responseEntity = loginController.me(request);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(usuario, responseEntity.getBody());
    }
}
