package org.example.gestionlogistica.Servicios;

import jakarta.servlet.http.HttpServletRequest;
import org.example.gestionlogistica.Configuracion.Constantes.Constantes;
import org.example.gestionlogistica.Configuracion.Exceptiones.UsuarioNoEncontradoException;
import org.example.gestionlogistica.Configuracion.JwtUtils;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Repositorios.ClienteRepository;
import org.example.gestionlogistica.Servicios.ServiceImplement.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;

@Service
public class LoginServiceImpl implements LoginService {

    private final ClienteRepository clienteRepository;
    private final JwtUtils jwtUtils;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public LoginServiceImpl(ClienteRepository clienteRepository, JwtUtils jwtUtils, PasswordEncoder passwordEncoder) {
        this.clienteRepository = clienteRepository;
        this.jwtUtils = jwtUtils;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public String login(String correo, String contrasena) throws AuthenticationException {
        Cliente cliente = clienteRepository.findByCorreo(correo);
        if (cliente == null || !passwordEncoder.matches(contrasena, cliente.getContrasena())) {
            throw new UsuarioNoEncontradoException(Constantes.USUARIO_CLAVE_INCORRECTOS, HttpStatus.UNAUTHORIZED);
        }
        return jwtUtils.generateToken(cliente);
    }

    @Override
    public Cliente getCurrentUser(HttpServletRequest request) {
        String token = jwtUtils.getTokenFromRequest(request);
        String email = jwtUtils.getUserNameFromToken(token);
        return clienteRepository.findByNombreUsuario(email);
    }
}
