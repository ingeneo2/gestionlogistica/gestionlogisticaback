package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Repositorios.EnvioTerrestreRepository;
import org.example.gestionlogistica.Servicios.ServiceImplement.GeneradorNumeroGuiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class GeneradorNumeroGuiaServiceImpl implements GeneradorNumeroGuiaService {

    private final EnvioTerrestreRepository envioTerrestreRepository;

    @Autowired
    public GeneradorNumeroGuiaServiceImpl(EnvioTerrestreRepository envioTerrestreRepository) {
        this.envioTerrestreRepository = envioTerrestreRepository;
    }

    @Override
    public String generateUniqueGuid() {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        while (envioTerrestreRepository.existsByNumeroGuia(uuid)) {
            uuid = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        }
        return uuid.substring(0, 10);
    }
}
