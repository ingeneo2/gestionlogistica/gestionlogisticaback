package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Configuracion.Constantes.Constantes;
import org.example.gestionlogistica.Configuracion.Exceptiones.BodegaNotFoundException;
import org.example.gestionlogistica.Configuracion.Exceptiones.ClienteNotFoundException;
import org.example.gestionlogistica.Configuracion.Exceptiones.ProductoNotFoundException;
import org.example.gestionlogistica.Modelo.DTO.BodegaDTO;
import org.example.gestionlogistica.Modelo.DTO.ClienteDTO;
import org.example.gestionlogistica.Modelo.DTO.EnvioTerrestreDTO;
import org.example.gestionlogistica.Modelo.DTO.ProductoDTO;
import org.example.gestionlogistica.Modelo.Entidades.Bodega;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Modelo.Entidades.EnvioTerrestre;
import org.example.gestionlogistica.Modelo.Entidades.Producto;
import org.example.gestionlogistica.Repositorios.BodegaRepository;
import org.example.gestionlogistica.Repositorios.ClienteRepository;
import org.example.gestionlogistica.Repositorios.EnvioTerrestreRepository;
import org.example.gestionlogistica.Repositorios.ProductoRepository;
import org.example.gestionlogistica.Servicios.ServiceImplement.EnvioTerrestreService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class EnvioTerrestreServiceImpl implements EnvioTerrestreService {
    @Autowired
    private EnvioTerrestreRepository envioTerrestreRepository;

    @Autowired
    private GeneradorNumeroGuiaServiceImpl generadorNumeroGuia;

    @Autowired
    private DescuentoServiceImpl calculadoraDescuento;

    @Autowired
    private BodegaRepository bodegaRepository;

    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<EnvioTerrestreDTO> getAllEnviosTerrestres() {
        List<EnvioTerrestre> enviosTerrestres = envioTerrestreRepository.findAll();
        return enviosTerrestres.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public EnvioTerrestreDTO getEnvioTerrestreById(Long id) {
        EnvioTerrestre envioTerrestre = envioTerrestreRepository.findById(id).orElse(null);
        return convertToDto(envioTerrestre);
    }


    public EnvioTerrestreDTO saveEnvioTerrestre(EnvioTerrestreDTO envioTerrestreDTO) {
        envioTerrestreDTO.setNumeroGuia(generadorNumeroGuia.generateUniqueGuid());
        envioTerrestreDTO.setDescuentoOtorgado(Double.parseDouble(calculadoraDescuento.calcularPrecioConDescuento(envioTerrestreDTO.getPrecioEnvio(), envioTerrestreDTO.getCantidad(), "terrestre")));

        Cliente cliente = convertClienteByName(envioTerrestreDTO.getCliente().getNombre());
        Bodega bodega = convertBodegaByName(envioTerrestreDTO.getBodega().getNombre(), envioTerrestreDTO.getBodega().getUbicacion());
        Producto producto = convertProductoByName(envioTerrestreDTO.getProducto().getNombre());

        envioTerrestreDTO.setCliente(convertToDto(cliente));
        envioTerrestreDTO.setProducto(convertToDto(producto));
        envioTerrestreDTO.setBodega(convertToDto(bodega));

        EnvioTerrestre envioTerrestre = convertToEntity(envioTerrestreDTO);
        envioTerrestre = envioTerrestreRepository.save(envioTerrestre);
        return convertToDto(envioTerrestre);
    }

    public void deleteEnvioTerrestre(Long id) {
        envioTerrestreRepository.deleteById(id);
    }
    public Cliente convertClienteByName(String nombreCliente) {
        Cliente cliente = clienteRepository.findByNombre(nombreCliente);
        if (cliente == null) {
            throw new ClienteNotFoundException(Constantes.CLIENTE_NO_ENCONTRADO_NOMBRE + nombreCliente);
        }
        return cliente;
    }

    public Producto convertProductoByName(String nombreProducto) {
        Producto producto = productoRepository.findByNombre(nombreProducto);
        if (producto == null) {
            throw new ProductoNotFoundException(Constantes.PRODUCTO_NO_ENCONTRADO_NOMBRE + nombreProducto);
        }
        return producto;
    }

    public Bodega convertBodegaByName(String nombreBodega, String ubicacion) {
        Bodega bodega = bodegaRepository.findByNombreAndUbicacion(nombreBodega, ubicacion);
        if (bodega == null) {
            throw new BodegaNotFoundException(Constantes.BODEGA_NO_ENCONTRADO_NOMBRE + nombreBodega + Constantes.UBICACION + ubicacion);
        }
        return bodega;
    }
    private EnvioTerrestreDTO convertToDto(EnvioTerrestre envioTerrestre) {
        return modelMapper.map(envioTerrestre, EnvioTerrestreDTO.class);
    }

    private ClienteDTO convertToDto(Cliente cliente) {
       return modelMapper.map(cliente, ClienteDTO.class);
    }

    private ProductoDTO convertToDto(Producto producto) {
        return modelMapper.map(producto, ProductoDTO.class);
    }

    private BodegaDTO convertToDto(Bodega bodega) {
        return modelMapper.map(bodega, BodegaDTO.class);
    }

    private EnvioTerrestre convertToEntity(EnvioTerrestreDTO envioTerrestreDTO) {
        return modelMapper.map(envioTerrestreDTO, EnvioTerrestre.class);
    }
}

