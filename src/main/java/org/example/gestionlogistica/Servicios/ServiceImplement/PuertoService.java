package org.example.gestionlogistica.Servicios.ServiceImplement;

import org.example.gestionlogistica.Modelo.DTO.PuertoDTO;

import java.util.List;

public interface PuertoService{
    List<PuertoDTO> getAllPuertos();
    PuertoDTO getPuertoById(Long id);
    PuertoDTO savePuerto(PuertoDTO puertoDTO);
    void deletePuerto(Long id);
}
