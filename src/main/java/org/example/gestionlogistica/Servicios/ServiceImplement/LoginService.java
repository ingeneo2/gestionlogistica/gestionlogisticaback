package org.example.gestionlogistica.Servicios.ServiceImplement;

import jakarta.servlet.http.HttpServletRequest;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;

import javax.naming.AuthenticationException;

public interface LoginService {
    String login(String correo, String contrasena) throws AuthenticationException;
    Cliente getCurrentUser(HttpServletRequest request);
}
