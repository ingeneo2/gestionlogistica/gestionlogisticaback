package org.example.gestionlogistica.Servicios.ServiceImplement;

import org.example.gestionlogistica.Modelo.DTO.EnvioTerrestreDTO;

import java.util.List;

public interface EnvioTerrestreService {
    List<EnvioTerrestreDTO> getAllEnviosTerrestres();

    EnvioTerrestreDTO getEnvioTerrestreById(Long id);

    EnvioTerrestreDTO saveEnvioTerrestre(EnvioTerrestreDTO envioTerrestreDTO);

    void deleteEnvioTerrestre(Long id);
}
