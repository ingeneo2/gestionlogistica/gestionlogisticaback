package org.example.gestionlogistica.Servicios.ServiceImplement;

public interface DescuentoService {
    String calcularPrecioConDescuento(String precioEnvio, int cantidad, String tipoEnvio);
}
