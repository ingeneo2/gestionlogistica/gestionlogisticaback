package org.example.gestionlogistica.Servicios.ServiceImplement;


import org.example.gestionlogistica.Modelo.DTO.EnvioMaritimoDTO;

import java.util.List;

public interface EnvioMaritimoService {
    List<EnvioMaritimoDTO> getAllEnviosMaritimos();
    EnvioMaritimoDTO getEnvioMaritimoById(Long id);
    EnvioMaritimoDTO saveEnvioMaritimo(EnvioMaritimoDTO envioMaritimoDTO);
    void deleteEnvioMaritimo(Long id);
}
