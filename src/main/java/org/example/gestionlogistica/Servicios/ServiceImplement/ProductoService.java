package org.example.gestionlogistica.Servicios.ServiceImplement;

import org.example.gestionlogistica.Modelo.DTO.ProductoDTO;

import java.util.List;

public interface ProductoService {
    List<ProductoDTO> getAllProductos();
    ProductoDTO getProductoById(Long id);
    ProductoDTO saveProducto(ProductoDTO productoDTO);
    void deleteProducto(Long id);
}
