package org.example.gestionlogistica.Servicios.ServiceImplement;

import org.example.gestionlogistica.Modelo.DTO.ClienteDTO;

import java.util.List;

public interface ClienteService {
    List<ClienteDTO> getAllClientes();
    ClienteDTO getClienteById(Long id);
    ClienteDTO saveCliente(ClienteDTO clienteDTO);
    void deleteCliente(Long id);
}
