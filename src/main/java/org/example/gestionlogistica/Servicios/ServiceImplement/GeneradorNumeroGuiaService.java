package org.example.gestionlogistica.Servicios.ServiceImplement;

public interface GeneradorNumeroGuiaService {
    String generateUniqueGuid();
}
