package org.example.gestionlogistica.Servicios.ServiceImplement;

import org.example.gestionlogistica.Modelo.DTO.BodegaDTO;

import java.util.List;

public interface BodegaService {
    List<BodegaDTO> getAllBodegas();
    BodegaDTO getBodegaById(Long id);
    BodegaDTO saveBodega(BodegaDTO bodegaDTO);
    void deleteBodega(Long id);
}
