package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Configuracion.Constantes.Constantes;
import org.example.gestionlogistica.Servicios.ServiceImplement.DescuentoService;
import org.springframework.stereotype.Service;

@Service
public class DescuentoServiceImpl implements DescuentoService {
    @Override
    public String calcularPrecioConDescuento(String precioEnvio, int cantidad, String tipoEnvio) {
        double precio = Double.parseDouble(precioEnvio);
        double porcentajeDescuento = 0.0;

        if (tipoEnvio.equalsIgnoreCase(Constantes.TERRESTRE)) {
            porcentajeDescuento = 0.05;
        } else if (tipoEnvio.equalsIgnoreCase(Constantes.MARITIMO)) {
            porcentajeDescuento = 0.03;
        }

        if (cantidad > 10) {
            double descuento = precio * porcentajeDescuento;
            precio -= descuento;
            return String.valueOf(precio);
        }
        return precioEnvio;
    }
}
