package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Modelo.DTO.PuertoDTO;
import org.example.gestionlogistica.Modelo.Entidades.Puerto;
import org.example.gestionlogistica.Repositorios.PuertoRepository;
import org.example.gestionlogistica.Servicios.ServiceImplement.PuertoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PuertoServiceImpl implements PuertoService {
    private final PuertoRepository puertoRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public PuertoServiceImpl(PuertoRepository puertoRepository, ModelMapper modelMapper) {
        this.puertoRepository = puertoRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<PuertoDTO> getAllPuertos() {
        List<Puerto> puertos = puertoRepository.findAll();
        return puertos.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public PuertoDTO getPuertoById(Long id) {
        Puerto puerto = puertoRepository.findById(id).orElse(null);
        return convertToDto(puerto);
    }

    @Override
    public PuertoDTO savePuerto(PuertoDTO puertoDTO) {
        Puerto puerto = convertToEntity(puertoDTO);
        puerto = puertoRepository.save(puerto);
        return convertToDto(puerto);
    }

    @Override
    public void deletePuerto(Long id) {
        puertoRepository.deleteById(id);
    }

    private PuertoDTO convertToDto(Puerto puerto) {
        return modelMapper.map(puerto, PuertoDTO.class);
    }

    private Puerto convertToEntity(PuertoDTO puertoDTO) {
        return modelMapper.map(puertoDTO, Puerto.class);
    }
}
