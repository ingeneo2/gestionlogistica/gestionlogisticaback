package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Configuracion.Constantes.Constantes;
import org.example.gestionlogistica.Configuracion.Exceptiones.ClienteNotFoundException;
import org.example.gestionlogistica.Configuracion.Exceptiones.ProductoNotFoundException;
import org.example.gestionlogistica.Configuracion.Exceptiones.PuertoNotFoundException;
import org.example.gestionlogistica.Modelo.DTO.ClienteDTO;
import org.example.gestionlogistica.Modelo.DTO.EnvioMaritimoDTO;
import org.example.gestionlogistica.Modelo.DTO.ProductoDTO;
import org.example.gestionlogistica.Modelo.DTO.PuertoDTO;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Modelo.Entidades.EnvioMaritimo;
import org.example.gestionlogistica.Modelo.Entidades.Producto;
import org.example.gestionlogistica.Modelo.Entidades.Puerto;
import org.example.gestionlogistica.Repositorios.ClienteRepository;
import org.example.gestionlogistica.Repositorios.EnvioMaritimoRepository;
import org.example.gestionlogistica.Repositorios.ProductoRepository;
import org.example.gestionlogistica.Repositorios.PuertoRepository;
import org.example.gestionlogistica.Servicios.ServiceImplement.EnvioMaritimoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EnvioMaritimoServiceImpl implements EnvioMaritimoService {
    @Autowired
    private EnvioMaritimoRepository envioMaritimoRepository;

    @Autowired
    private GeneradorNumeroGuiaServiceImpl generadorNumeroGuia;

    @Autowired
    private DescuentoServiceImpl calculadoraDescuento;

    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private PuertoRepository puertoRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<EnvioMaritimoDTO> getAllEnviosMaritimos() {
        List<EnvioMaritimo> enviosMaritimos = envioMaritimoRepository.findAll();
        return enviosMaritimos.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public EnvioMaritimoDTO getEnvioMaritimoById(Long id) {
        EnvioMaritimo envioMaritimo = envioMaritimoRepository.findById(id).orElse(null);
        return convertToDto(envioMaritimo);
    }

    @Override
    public EnvioMaritimoDTO saveEnvioMaritimo(EnvioMaritimoDTO envioMaritimoDTO) {
        envioMaritimoDTO.setNumeroGuia(generadorNumeroGuia.generateUniqueGuid());
        envioMaritimoDTO.setDescuentoOtorgado(Double.parseDouble(calculadoraDescuento.calcularPrecioConDescuento(envioMaritimoDTO.getPrecioEnvio(),envioMaritimoDTO.getCantidad(), "maritimo")));

        Cliente cliente = convertClienteByName(envioMaritimoDTO.getCliente().getNombre());
        Puerto puerto = convertPuertoByName(envioMaritimoDTO.getPuerto().getNombre(), envioMaritimoDTO.getPuerto().getUbicacion());
        Producto producto = convertProductoByName(envioMaritimoDTO.getProducto().getNombre());

        envioMaritimoDTO.setCliente(convertToDto(cliente));
        envioMaritimoDTO.setProducto(convertToDto(producto));
        envioMaritimoDTO.setPuerto(convertToDto(puerto));

        EnvioMaritimo envioMaritimo = convertToEntity(envioMaritimoDTO);
        envioMaritimo = envioMaritimoRepository.save(envioMaritimo);
        return convertToDto(envioMaritimo);
    }

    @Override
    public void deleteEnvioMaritimo(Long id) {
        envioMaritimoRepository.deleteById(id);
    }

    private Cliente convertClienteByName(String nombreCliente) {
        Cliente cliente = clienteRepository.findByNombre(nombreCliente);
        if (cliente == null) {
            throw new ClienteNotFoundException(Constantes.CLIENTE_NO_ENCONTRADO_NOMBRE + nombreCliente);
        }
        return cliente;
    }

    private Producto convertProductoByName(String nombreProducto) {
        Producto producto = productoRepository.findByNombre(nombreProducto);
        if (producto == null) {
            throw new ProductoNotFoundException(Constantes.PRODUCTO_NO_ENCONTRADO_NOMBRE + nombreProducto);
        }
        return producto;
    }

    private Puerto convertPuertoByName(String nombrePuerto, String ubicacion) {
        Puerto puerto = puertoRepository.findByNombreAndUbicacion(nombrePuerto, ubicacion);
        if (puerto == null) {
            throw new PuertoNotFoundException(Constantes.PUERTO_NO_ENCONTRADO_NOMBRE + nombrePuerto + " o la ubicacion esta mal : " + ubicacion);
        }
        return puerto;
    }

    private EnvioMaritimoDTO convertToDto(EnvioMaritimo envioMaritimo) {
        return modelMapper.map(envioMaritimo, EnvioMaritimoDTO.class);
    }

    private ClienteDTO convertToDto(Cliente cliente) {
        return modelMapper.map(cliente, ClienteDTO.class);
    }

    private ProductoDTO convertToDto(Producto producto) {
        return modelMapper.map(producto, ProductoDTO.class);
    }

    private PuertoDTO convertToDto(Puerto puerto) {
        return modelMapper.map(puerto, PuertoDTO.class);
    }

    private EnvioMaritimo convertToEntity(EnvioMaritimoDTO envioMaritimoDTO) {
        return modelMapper.map(envioMaritimoDTO, EnvioMaritimo.class);
    }
}
