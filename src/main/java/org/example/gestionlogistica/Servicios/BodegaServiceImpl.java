package org.example.gestionlogistica.Servicios;

import org.example.gestionlogistica.Modelo.DTO.BodegaDTO;
import org.example.gestionlogistica.Modelo.Entidades.Bodega;
import org.example.gestionlogistica.Repositorios.BodegaRepository;
import org.example.gestionlogistica.Servicios.ServiceImplement.BodegaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BodegaServiceImpl implements BodegaService {
    @Autowired
    private BodegaRepository bodegaRepository;
    @Autowired
    private ModelMapper modelMapper;

    public List<BodegaDTO> getAllBodegas() {
        List<Bodega> bodegas = bodegaRepository.findAll();
        return bodegas.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public BodegaDTO getBodegaById(Long id) {
        Bodega bodega = bodegaRepository.findById(id).orElse(null);
        return convertToDto(bodega);
    }

    public BodegaDTO saveBodega(BodegaDTO bodegaDTO) {
        Bodega bodega = convertToEntity(bodegaDTO);
        if (bodega != null) {
            bodega = bodegaRepository.save(bodega);
            if (bodega != null) {
                return convertToDto(bodega);
            } else {
                throw new RuntimeException("Error al guardar la bodega en la base de datos");
            }
        } else {
            throw new IllegalArgumentException("El objeto BodegaDTO proporcionado es nulo");
        }
    }


    @Override
    public void deleteBodega(Long id) {
        bodegaRepository.deleteById(id);
    }

    private BodegaDTO convertToDto(Bodega bodega) {
        return modelMapper.map(bodega, BodegaDTO.class);
    }

    private Bodega convertToEntity(BodegaDTO bodegaDTO) {
        return modelMapper.map(bodegaDTO, Bodega.class);
    }
}

