package org.example.gestionlogistica.Controladores;

import jakarta.servlet.http.HttpServletRequest;
import org.example.gestionlogistica.Configuracion.AuthResponse;
import org.example.gestionlogistica.Modelo.DTO.ClienteDTO;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.example.gestionlogistica.Servicios.LoginServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class LoginController {

    @Autowired
    private LoginServiceImpl loginService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody ClienteDTO request) throws AuthenticationException {
        String token = loginService.login(request.getCorreo(), request.getContrasena());
        return ResponseEntity.ok(new AuthResponse(token));
    }

    @GetMapping("/me")
    public ResponseEntity<?> me(HttpServletRequest request) {
        Cliente usuario = loginService.getCurrentUser(request);
        return ResponseEntity.ok(usuario);
    }

    public char[] login(String username, String encryptedPass) {

        return new char[0];
    }
}
