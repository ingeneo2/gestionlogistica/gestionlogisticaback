package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.EnvioTerrestreDTO;
import org.example.gestionlogistica.Servicios.EnvioTerrestreServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/envios/terrestres")
public class EnvioTerrestreController {
    @Autowired
    private EnvioTerrestreServiceImpl envioTerrestreService;

    @GetMapping
    public ResponseEntity<List<EnvioTerrestreDTO>> getAllEnviosTerrestres() {
        List<EnvioTerrestreDTO> enviosTerrestres = envioTerrestreService.getAllEnviosTerrestres();
        return new ResponseEntity<>(enviosTerrestres, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EnvioTerrestreDTO> getEnvioTerrestreById(@PathVariable Long id) {
        EnvioTerrestreDTO envioTerrestreDTO = envioTerrestreService.getEnvioTerrestreById(id);
        return new ResponseEntity<>(envioTerrestreDTO, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EnvioTerrestreDTO> saveEnvioTerrestre(@RequestBody EnvioTerrestreDTO envioTerrestreDTO) {
        EnvioTerrestreDTO savedEnvioTerrestreDTO = envioTerrestreService.saveEnvioTerrestre(envioTerrestreDTO);
        return new ResponseEntity<>(savedEnvioTerrestreDTO, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEnvioTerrestre(@PathVariable Long id) {
        envioTerrestreService.deleteEnvioTerrestre(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

