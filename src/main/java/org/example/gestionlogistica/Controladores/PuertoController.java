package org.example.gestionlogistica.Controladores;


import org.example.gestionlogistica.Modelo.DTO.PuertoDTO;
import org.example.gestionlogistica.Servicios.PuertoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/puertos")
public class PuertoController {
    @Autowired
    private PuertoServiceImpl puertoService;

    @GetMapping
    public ResponseEntity<List<PuertoDTO>> getAllPuertos() {
        List<PuertoDTO> puertos = puertoService.getAllPuertos();
        return new ResponseEntity<>(puertos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PuertoDTO> getPuertoById(@PathVariable Long id) {
        PuertoDTO puertoDTO = puertoService.getPuertoById(id);
        return new ResponseEntity<>(puertoDTO, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PuertoDTO> savePuerto(@RequestBody PuertoDTO puertoDTO) {
        PuertoDTO savedPuertoDTO = puertoService.savePuerto(puertoDTO);
        return new ResponseEntity<>(savedPuertoDTO, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePuerto(@PathVariable Long id) {
        puertoService.deletePuerto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

