package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.EnvioMaritimoDTO;
import org.example.gestionlogistica.Servicios.EnvioMaritimoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/envios/maritimos")
public class EnvioMaritimoController {
    @Autowired
    private EnvioMaritimoServiceImpl envioMaritimoService;

    @GetMapping
    public ResponseEntity<List<EnvioMaritimoDTO>> getAllEnviosMaritimos() {
        List<EnvioMaritimoDTO> enviosMaritimos = envioMaritimoService.getAllEnviosMaritimos();
        return new ResponseEntity<>(enviosMaritimos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EnvioMaritimoDTO> getEnvioMaritimoById(@PathVariable Long id) {
        EnvioMaritimoDTO envioMaritimoDTO = envioMaritimoService.getEnvioMaritimoById(id);
        return new ResponseEntity<>(envioMaritimoDTO, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EnvioMaritimoDTO> saveEnvioMaritimo(@RequestBody EnvioMaritimoDTO envioMaritimoDTO) {
        EnvioMaritimoDTO savedEnvioMaritimoDTO = envioMaritimoService.saveEnvioMaritimo(envioMaritimoDTO);
        return new ResponseEntity<>(savedEnvioMaritimoDTO, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEnvioMaritimo(@PathVariable Long id) {
        envioMaritimoService.deleteEnvioMaritimo(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

