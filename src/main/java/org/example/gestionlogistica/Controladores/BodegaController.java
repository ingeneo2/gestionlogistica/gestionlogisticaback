package org.example.gestionlogistica.Controladores;

import org.example.gestionlogistica.Modelo.DTO.BodegaDTO;
import org.example.gestionlogistica.Servicios.BodegaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bodegas")
public class BodegaController {
    @Autowired
    private BodegaServiceImpl bodegaService;

    @GetMapping
    public ResponseEntity<List<BodegaDTO>> getAllBodegas() {
        List<BodegaDTO> bodegas = bodegaService.getAllBodegas();
        return new ResponseEntity<>(bodegas, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BodegaDTO> getBodegaById(@PathVariable Long id) {
        BodegaDTO bodegaDTO = bodegaService.getBodegaById(id);
        return new ResponseEntity<>(bodegaDTO, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<BodegaDTO> saveBodega(@RequestBody BodegaDTO bodegaDTO) {
        BodegaDTO savedBodegaDTO = bodegaService.saveBodega(bodegaDTO);
        return new ResponseEntity<>(savedBodegaDTO, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBodega(@PathVariable Long id) {
        bodegaService.deleteBodega(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

