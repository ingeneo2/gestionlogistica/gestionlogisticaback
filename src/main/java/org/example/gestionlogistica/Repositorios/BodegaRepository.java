package org.example.gestionlogistica.Repositorios;

import org.example.gestionlogistica.Modelo.Entidades.Bodega;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BodegaRepository extends JpaRepository<Bodega, Long> {
    @Query("SELECT b FROM Bodega b WHERE LOWER(b.nombre) = LOWER(?1) AND LOWER(b.ubicacion) = LOWER(?2)")
    Bodega findByNombreAndUbicacion(String nombre, String ubicacion);
}

