package org.example.gestionlogistica.Repositorios;

import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    @Query("SELECT c FROM Cliente c WHERE LOWER(c.nombre) = LOWER(?1)")
    Cliente findByNombre(String nombre);
    @Query("SELECT c FROM Cliente c WHERE LOWER(c.nombreUsuario) = LOWER(?1) AND c.contrasena = ?2")
    Cliente findByNombreUsuarioAndContrasena(String nombreUsuario, String contrasena);

    Cliente findByNombreUsuario(String username);
    Cliente findByCorreo(String correo);
}
