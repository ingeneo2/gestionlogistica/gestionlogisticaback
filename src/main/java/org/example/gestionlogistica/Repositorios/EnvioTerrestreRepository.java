package org.example.gestionlogistica.Repositorios;

import org.example.gestionlogistica.Modelo.Entidades.EnvioTerrestre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnvioTerrestreRepository extends JpaRepository<EnvioTerrestre, Long> {
    boolean existsByNumeroGuia(String numeroGuia);
}

