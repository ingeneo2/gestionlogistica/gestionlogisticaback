package org.example.gestionlogistica.Repositorios;


import org.example.gestionlogistica.Modelo.Entidades.Puerto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PuertoRepository extends JpaRepository<Puerto, Long> {
    @Query("SELECT b FROM Puerto b WHERE LOWER(b.nombre) = LOWER(?1) AND LOWER(b.ubicacion) = LOWER(?2)")
    Puerto findByNombreAndUbicacion(String nombre, String ubicacion);
}

