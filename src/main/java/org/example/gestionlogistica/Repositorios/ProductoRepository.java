package org.example.gestionlogistica.Repositorios;

import org.example.gestionlogistica.Modelo.Entidades.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductoRepository extends JpaRepository<Producto, Long> {
    @Query("SELECT p FROM Producto p WHERE LOWER(p.nombre) = LOWER(?1)")
    Producto findByNombre(String nombre);
}

