package org.example.gestionlogistica.Repositorios;

import org.example.gestionlogistica.Modelo.Entidades.EnvioMaritimo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnvioMaritimoRepository extends JpaRepository<EnvioMaritimo, Long> {
    boolean existsByNumeroGuia(String numeroGuia);
}

