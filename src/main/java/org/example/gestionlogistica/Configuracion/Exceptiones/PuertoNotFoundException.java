package org.example.gestionlogistica.Configuracion.Exceptiones;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PuertoNotFoundException extends RuntimeException {
    public PuertoNotFoundException(String message) {
        super(message);
    }
}
