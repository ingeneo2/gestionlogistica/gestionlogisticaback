package org.example.gestionlogistica.Configuracion.Exceptiones;

import org.springframework.http.HttpStatus;

public class UsuarioNoEncontradoException extends RuntimeException {
    public UsuarioNoEncontradoException(String message, HttpStatus notFound) {
        super(message);
    }

    public UsuarioNoEncontradoException() {

    }
}

