package org.example.gestionlogistica.Configuracion;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.example.gestionlogistica.Configuracion.Constantes.Constantes;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class JWTAuthtenticationConfig {
    public String getJWTToken(String username) {
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList(Constantes.ROL);

        String token = Jwts
                .builder()
                .setId(Constantes.ID)
                .setSubject(username)
                .claim(Constantes.AUTHORITIES,
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + Constantes.TOKEN_EXPIRATION_TIME))
                .signWith(Constantes.getSigningKey(Constantes.SUPER_SECRET_KEY),  SignatureAlgorithm.HS512).compact();

        return Constantes.BEARER + token;
    }
}
