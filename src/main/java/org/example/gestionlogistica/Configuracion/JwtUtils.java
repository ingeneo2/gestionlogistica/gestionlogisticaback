package org.example.gestionlogistica.Configuracion;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.servlet.http.HttpServletRequest;
import org.example.gestionlogistica.Configuracion.Constantes.Constantes;
import org.example.gestionlogistica.Modelo.Entidades.Cliente;
import org.springframework.context.annotation.Configuration;

import java.security.SecureRandom;
import java.util.Date;

@Configuration
public class JwtUtils {

    private static final byte[] SECRET_KEY;

    static {
        SecureRandom secureRandom = new SecureRandom();
        SECRET_KEY = new byte[64]; // 512 bits (recommended for HS512)
        secureRandom.nextBytes(SECRET_KEY);
    }

    public static String generateToken(Cliente user) {
        return Jwts.builder()
                .setSubject(user.getCorreo())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10)) // 10 hours
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    public static String getTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(Constantes.AUTHORIZATION);

        return bearerToken;
    }

    public static String getUserNameFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }
}
