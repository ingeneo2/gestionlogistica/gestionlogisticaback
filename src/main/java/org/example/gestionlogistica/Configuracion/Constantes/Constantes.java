package org.example.gestionlogistica.Configuracion.Constantes;

import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import java.nio.charset.StandardCharsets;
import java.security.Key;

public class Constantes {
    // Spring Security
    public static final String LOGIN_URL = "/login";
    public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
    public static final String TOKEN_BEARER_PREFIX = "Bearer ";
    public static final String CREAR_CLIENTE = "/api/clientes";
    public static final String CREAR_BODEGA = "/api/bodegas";
    public static final String CREAR_PUERTO = "/api/puertos";
    public static final String CREAR_PRODUCTO = "/api/productos";
    public static final String CREAR_ENVIOS_TERRESTRES = "/api/envios/terrestres";
    public static final String CREAR_ENVIOS_MARITIMOS = "/api/envios/maritimos";

    // JWT
    public static final String SUPER_SECRET_KEY = "ZnJhc2VzbGFyZ2FzcGFyYWNvbG9jYXJjb21vY2xhdmVlbnVucHJvamVjdG9kZWVtZXBsb3BhcmFqd3Rjb25zcHJpbmdzZWN1cml0eQ==bWlwcnVlYmFkZWVqbXBsb3BhcmFiYXNlNjQ=";
    public static final long TOKEN_EXPIRATION_TIME = 864_000_000; // 10 day

    public static final String AUTHORITIES = "authorities";
    public static final String AUTHORIZATION = "Authorization";
    public static final String ESPACIO = "";
    public static final String ID = "carlos";
    public static final String ROL = "ROLE_USER";
    public static final String BEARER = "Bearer ";

    //Cors
    public static final String URL_ANGULAR = "http://localhost:4200";
    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String API = "/api/**";
    public static final String ENCABEZADO = "*";

    //Controladores y servicios

    public static final String INICIO_EXITOSO = "Inicio de sesión exitoso";
    public static final String USUARIO_CLAVE_INCORRECTOS = "Nombre de usuario o contraseña incorrectos";
    public static final String USUARIO_NO_ENCONTRADO = "Usuario no encontrado";
    public static final String MULTIPLES_CLIENTES = "Se encontraron múltiples clientes con las mismas credenciales";
    public static final String CLIENTE_NO_ENCONTRADO_NOMBRE = "Cliente no encontrado con el nombre: ";
    public static final String PRODUCTO_NO_ENCONTRADO_NOMBRE = "Producto no encontrado con el nombre: ";
    public static final String PUERTO_NO_ENCONTRADO_NOMBRE = "Puerto no encontrado con el nombre: ";
    public static final String BODEGA_NO_ENCONTRADO_NOMBRE = "Bodega no encontrado con el nombre: ";
    public static final String UBICACION = " o la ubicacion esta mal : ";

    public static final String TERRESTRE = "terrestre";
    public static final String MARITIMO = "maritimo";
    //Errores

    public static final String INTERNAL_SERVER = "Error interno del servidor";


    public static Key getSigningKeyB64(String secret) {
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        return Keys.hmacShaKeyFor(keyBytes);
    }
    public static Key getSigningKey(String secret) {
        byte[] keyBytes = secret.getBytes(StandardCharsets.UTF_8);
        return Keys.hmacShaKeyFor(keyBytes);
    }

}
