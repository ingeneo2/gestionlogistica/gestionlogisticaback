package org.example.gestionlogistica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionLogisticaApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestionLogisticaApplication.class, args);
    }

}
