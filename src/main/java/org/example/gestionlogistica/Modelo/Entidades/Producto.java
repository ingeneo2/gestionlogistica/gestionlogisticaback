package org.example.gestionlogistica.Modelo.Entidades;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String descripcion;
    private String tipo;

    @OneToMany(mappedBy = "producto", cascade = CascadeType.ALL)
    private List<EnvioTerrestre> enviosTerrestres;

    @OneToMany(mappedBy = "producto", cascade = CascadeType.ALL)
    private List<EnvioMaritimo> enviosMaritimos;

    public Producto(long l, String producto1, String descripción1, String tipo1) {
    }

    public Producto(Long o, String producto1, String descripción1, String tipo1) {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<EnvioTerrestre> getEnviosTerrestres() {
        return enviosTerrestres;
    }

    public void setEnviosTerrestres(List<EnvioTerrestre> enviosTerrestres) {
        this.enviosTerrestres = enviosTerrestres;
    }

    public List<EnvioMaritimo> getEnviosMaritimos() {
        return enviosMaritimos;
    }

    public void setEnviosMaritimos(List<EnvioMaritimo> enviosMaritimos) {
        this.enviosMaritimos = enviosMaritimos;
    }
}

