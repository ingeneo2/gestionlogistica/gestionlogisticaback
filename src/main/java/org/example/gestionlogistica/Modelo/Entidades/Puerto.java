package org.example.gestionlogistica.Modelo.Entidades;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Puerto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String ubicacion;
    private int capacidad;

    @OneToMany(mappedBy = "puerto", cascade = CascadeType.ALL)
    private List<EnvioMaritimo> enviosMaritimos;

    public Puerto(long l, String puerto1, String ubicacion1, int i) {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public List<EnvioMaritimo> getEnviosMaritimos() {
        return enviosMaritimos;
    }

    public void setEnviosMaritimos(List<EnvioMaritimo> enviosMaritimos) {
        this.enviosMaritimos = enviosMaritimos;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }
}

