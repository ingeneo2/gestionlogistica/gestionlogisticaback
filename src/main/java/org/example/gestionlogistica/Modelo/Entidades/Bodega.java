package org.example.gestionlogistica.Modelo.Entidades;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Bodega {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String ubicacion;
    private int capacidad;

    @OneToMany(mappedBy = "bodega", cascade = CascadeType.ALL)
    private List<EnvioTerrestre> enviosTerrestres;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public List<EnvioTerrestre> getEnviosTerrestres() {
        return enviosTerrestres;
    }

    public void setEnviosTerrestres(List<EnvioTerrestre> enviosTerrestres) {
        this.enviosTerrestres = enviosTerrestres;
    }
}

