package org.example.gestionlogistica.Modelo.Entidades;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String direccion;
    private String telefono;
    private String nombreUsuario;
    private String contrasena;
    @Email
    private String correo;

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<EnvioTerrestre> enviosTerrestres;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<EnvioMaritimo> enviosMaritimos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<EnvioTerrestre> getEnviosTerrestres() {
        return enviosTerrestres;
    }

    public void setEnviosTerrestres(List<EnvioTerrestre> enviosTerrestres) {
        this.enviosTerrestres = enviosTerrestres;
    }

    public List<EnvioMaritimo> getEnviosMaritimos() {
        return enviosMaritimos;
    }

    public void setEnviosMaritimos(List<EnvioMaritimo> enviosMaritimos) {
        this.enviosMaritimos = enviosMaritimos;
    }
}
