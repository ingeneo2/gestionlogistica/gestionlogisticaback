package org.example.gestionlogistica.Modelo.DTO;


public class PuertoDTO {
    private Long id;
    private String nombre;
    private String ubicacion;
    private int capacidad;

    public PuertoDTO(long l, String s, String s1, int i) {
    }

    public PuertoDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }
}

