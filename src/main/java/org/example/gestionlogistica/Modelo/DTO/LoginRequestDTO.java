package org.example.gestionlogistica.Modelo.DTO;

public class LoginRequestDTO {
    private String correo;
    private String contrasena;

    public LoginRequestDTO(String correo, String contrasena) {
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
