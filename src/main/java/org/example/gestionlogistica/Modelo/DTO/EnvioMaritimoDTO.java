package org.example.gestionlogistica.Modelo.DTO;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EnvioMaritimoDTO {
    private Long id;
    private ClienteDTO cliente;
    private ProductoDTO producto;
    private PuertoDTO puerto;
    private String tipoProducto;
    private int cantidad;
    private LocalDate fechaRegistro;
    private LocalDate fechaEntrega;
    private String precioEnvio;
    private String numeroFlota;
    private String numeroGuia;
    private double precioNormal;
    private double descuentoOtorgado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public ProductoDTO getProducto() {
        return producto;
    }

    public void setProducto(ProductoDTO producto) {
        this.producto = producto;
    }

    public PuertoDTO getPuerto() {
        return puerto;
    }

    public void setPuerto(PuertoDTO puerto) {
        this.puerto = puerto;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public LocalDate getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDate fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getPrecioEnvio() {
        return precioEnvio;
    }

    public void setPrecioEnvio(String precioEnvio) {
        this.precioEnvio = precioEnvio;
    }

    public String getNumeroFlota() {
        return numeroFlota;
    }

    public void setNumeroFlota(String numeroFlota) {
        this.numeroFlota = numeroFlota;
    }

    public String getNumeroGuia() {
        return numeroGuia;
    }

    public void setNumeroGuia(String numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    public double getPrecioNormal() {
        return precioNormal;
    }

    public void setPrecioNormal(double precioNormal) {
        this.precioNormal = precioNormal;
    }

    public double getDescuentoOtorgado() {
        return descuentoOtorgado;
    }

    public void setDescuentoOtorgado(double descuentoOtorgado) {
        this.descuentoOtorgado = descuentoOtorgado;
    }
}
